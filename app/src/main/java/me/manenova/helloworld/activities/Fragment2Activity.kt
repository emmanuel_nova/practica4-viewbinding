package me.manenova.helloworld.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import me.manenova.helloworld.R
import me.manenova.helloworld.databinding.ActivityFragment2Binding

class Fragment2Activity : AppCompatActivity() {

    private lateinit var binding: ActivityFragment2Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFragment2Binding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}