package me.manenova.helloworld.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import me.manenova.helloworld.R
import androidx.fragment.app.commit;
import me.manenova.helloworld.databinding.ActivityFragmentBinding
import me.manenova.helloworld.fragments.FirstFragment
import me.manenova.helloworld.fragments.SecondFragment

class FragmentActivity : AppCompatActivity() {

    private lateinit var binding: ActivityFragmentBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFragmentBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_fragment,menu)
        return super.onCreateOptionsMenu(menu)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home -> finish()
            R.id.menu_item_fragment1 -> {
                supportFragmentManager.commit {
                    replace(R.id.fragment_container_view,FirstFragment())
                }
            }
            R.id.menu_item_fragment2-> {
                supportFragmentManager.commit {
                    replace(R.id.fragment_container_view,SecondFragment())
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

}